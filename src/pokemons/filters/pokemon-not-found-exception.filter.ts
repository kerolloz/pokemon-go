import { ArgumentsHost, Catch, ExceptionFilter } from '@nestjs/common';
import { EntityNotFoundError } from 'typeorm';

@Catch(EntityNotFoundError)
export class PokemonNotFoundExceptionFilter implements ExceptionFilter {
  catch(_: EntityNotFoundError, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();
    const id = request.params.id;

    return response.status(404).json({
      statusCode: 404,
      message: `Pokemon with id ${id} not found`,
      error: 'Not Found',
    });
  }
}
