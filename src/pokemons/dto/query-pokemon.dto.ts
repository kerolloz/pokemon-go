import { PartialType, PickType } from '@nestjs/mapped-types';
import { CreatePokemonDto } from './create-pokemon.dto';

export class QueryPokemonDto extends PartialType(
  PickType(CreatePokemonDto, ['name', 'type1', 'pokedexNumber', 'weather1']),
) {}
