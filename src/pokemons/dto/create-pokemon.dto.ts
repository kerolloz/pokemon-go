import { IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';

export class CreatePokemonDto {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsNumber()
  @IsNotEmpty()
  pokedexNumber: number;

  @IsString()
  @IsNotEmpty()
  imgName: string;

  @IsNumber()
  @IsNotEmpty()
  generation: number;

  @IsString()
  @IsOptional()
  evolutionStage?: string;

  @IsNumber()
  @IsNotEmpty()
  evolved: number;

  @IsNumber()
  @IsOptional()
  familyID?: number;

  @IsNumber()
  @IsNotEmpty()
  crossGen: number;

  @IsString()
  @IsNotEmpty()
  type1: string;

  @IsString()
  @IsOptional()
  type2?: string;

  @IsString()
  @IsNotEmpty()
  weather1: string;

  @IsString()
  @IsOptional()
  weather2?: string;

  @IsNumber()
  @IsNotEmpty()
  statTotal: number;

  @IsNumber()
  @IsNotEmpty()
  atk: number;

  @IsNumber()
  @IsNotEmpty()
  def: number;

  @IsNumber()
  @IsNotEmpty()
  sta: number;

  @IsNumber()
  @IsNotEmpty()
  legendary: number;

  @IsNumber()
  @IsNotEmpty()
  aquireable: number;

  @IsNumber()
  @IsNotEmpty()
  spawns: number;

  @IsNumber()
  @IsNotEmpty()
  regional: number;

  @IsNumber()
  @IsNotEmpty()
  raidable: number;

  @IsNumber()
  @IsNotEmpty()
  hatchable: number;

  @IsNumber()
  @IsNotEmpty()
  shiny: number;

  @IsNumber()
  @IsNotEmpty()
  nest: number;

  @IsNumber()
  @IsNotEmpty()
  new: number;

  @IsNumber()
  @IsNotEmpty()
  notGettable: number;

  @IsNumber()
  @IsNotEmpty()
  futureEvolve: number;

  @IsNumber()
  @IsNotEmpty()
  cp40: number;

  @IsNumber()
  @IsNotEmpty()
  cp39: number;
}
