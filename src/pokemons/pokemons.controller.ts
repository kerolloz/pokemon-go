import {
  Body,
  Controller,
  DefaultValuePipe,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
  UseFilters,
} from '@nestjs/common';
import { EntityNotFoundError } from 'typeorm/error/EntityNotFoundError';
import { CreatePokemonDto } from './dto/create-pokemon.dto';
import { QueryPokemonDto } from './dto/query-pokemon.dto';
import { UpdatePokemonDto } from './dto/update-pokemon.dto';
import { PokemonNotFoundExceptionFilter } from './filters/pokemon-not-found-exception.filter';
import { PokemonsService } from './pokemons.service';
@UseFilters(PokemonNotFoundExceptionFilter)
@Controller('pokemons')
export class PokemonsController {
  constructor(private readonly pokemonsService: PokemonsService) {}

  @Post()
  async create(@Body() createPokemonDto: CreatePokemonDto) {
    return await this.pokemonsService.create(createPokemonDto);
  }

  @Get()
  findAll(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page = 1,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit = 10,
    @Query('q')
    q: QueryPokemonDto,
  ) {
    const options = {
      page,
      limit: Math.min(limit, 50),
    };

    return this.pokemonsService.paginate(options, q);
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    return await this.pokemonsService.findByIdOrFail(+id);
  }

  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updatePokemonDto: UpdatePokemonDto,
  ) {
    return await this.pokemonsService.update(+id, updatePokemonDto);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  async remove(@Param('id') id: string) {
    const isRemoved = await this.pokemonsService.remove(+id);
    if (!isRemoved) throw new EntityNotFoundError('Pokemon', id);
  }
}
