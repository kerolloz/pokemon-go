import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  IPaginationOptions,
  Pagination,
  paginate,
} from 'nestjs-typeorm-paginate';
import { Repository } from 'typeorm';
import { CreatePokemonDto } from './dto/create-pokemon.dto';
import { QueryPokemonDto } from './dto/query-pokemon.dto';
import { UpdatePokemonDto } from './dto/update-pokemon.dto';
import { Pokemon } from './entities/pokemon.entity';

@Injectable()
export class PokemonsService {
  constructor(
    @InjectRepository(Pokemon)
    private readonly pokemonsRepository: Repository<Pokemon>,
  ) {}

  async create(createPokemonDto: CreatePokemonDto) {
    return await this.pokemonsRepository.save(createPokemonDto);
  }

  async paginate(
    options: IPaginationOptions,
    query: QueryPokemonDto,
  ): Promise<Pagination<Pokemon>> {
    return paginate<Pokemon>(this.pokemonsRepository, options, {
      where: query,
    });
  }

  async findByIdOrFail(id: number): Promise<Pokemon> {
    return await this.pokemonsRepository.findOneByOrFail({ id });
  }

  async update(id: number, updatePokemonDto: UpdatePokemonDto) {
    const pokemon = await this.findByIdOrFail(id);
    return await this.pokemonsRepository.save({
      ...pokemon,
      ...updatePokemonDto,
    });
  }

  async remove(id: number): Promise<boolean> {
    const result = await this.pokemonsRepository.delete(id);
    return result.affected === 1;
  }
}
