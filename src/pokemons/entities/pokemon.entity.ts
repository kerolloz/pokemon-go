import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Pokemon {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ name: 'pokedex_number' })
  pokedexNumber: number;

  @Column({ name: 'img_name' })
  imgName: string;

  @Column()
  generation: number;

  @Column({
    nullable: true,
    name: 'evolution_stage',
  })
  evolutionStage?: string;

  @Column()
  evolved: number;

  @Column({
    nullable: true,
    name: 'family_id',
  })
  familyID?: number;

  @Column({
    name: 'cross_gen',
  })
  crossGen: number;

  @Column()
  type1: string;

  @Column({
    nullable: true,
  })
  type2?: string;

  @Column()
  weather1: string;

  @Column({
    nullable: true,
  })
  weather2?: string;

  @Column({
    name: 'stat_total',
  })
  statTotal: number;

  @Column()
  atk: number;

  @Column()
  def: number;

  @Column()
  sta: number;

  @Column()
  legendary: number;

  @Column()
  aquireable: number;

  @Column()
  spawns: number;

  @Column()
  regional: number;

  @Column()
  raidable: number;

  @Column()
  hatchable: number;

  @Column()
  shiny: number;

  @Column()
  nest: number;

  @Column()
  new: number;

  @Column({
    name: 'not_gettable',
  })
  notGettable: number;

  @Column({
    name: 'future_evolve',
  })
  futureEvolve: number;

  @Column()
  cp40: number;

  @Column()
  cp39: number;
}
