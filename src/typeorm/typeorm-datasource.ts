import { DataSource } from 'typeorm';

export const dataSource = new DataSource({
  type: 'mssql',
  host: process.env.DB_HOST || 'localhost',
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  entities: ['src/**/*.entity{.ts,.js}'],
  migrations: ['src/db/migrations/*{.ts,.js}', 'src/db/seeders/*{.ts,.js}'],
  extra: {
    trustServerCertificate: true,
  },
});
