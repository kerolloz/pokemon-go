import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { TypeOrmModuleOptions, TypeOrmOptionsFactory } from '@nestjs/typeorm';

@Injectable()
export class TypeOrmConfigService implements TypeOrmOptionsFactory {
  constructor(private readonly configService: ConfigService) {}

  createTypeOrmOptions(): TypeOrmModuleOptions {
    return {
      type: 'mssql',
      host: this.configService.get('DB_HOST', 'localhost'),
      username: this.configService.get('DB_USERNAME'),
      password: this.configService.get('DB_PASSWORD'),
      entities: ['dist/**/*.entity{.ts,.js}'],
      migrations: [
        'dist/db/migrations/*{.ts,.js}',
        'dist/db/seeders/*{.ts,.js}',
      ],
      migrationsRun: true,
      extra: {
        trustServerCertificate: true,
      },
    };
  }
}
