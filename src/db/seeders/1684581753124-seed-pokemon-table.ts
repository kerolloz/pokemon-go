import { Logger } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { join } from 'path';
import { MigrationInterface, QueryRunner } from 'typeorm';
import * as XLSX from 'xlsx';
import { CreatePokemonDto } from '../../pokemons/dto/create-pokemon.dto';
import { Pokemon } from '../../pokemons/entities/pokemon.entity';

export class SeedPokemonTable1684581753124 implements MigrationInterface {
  private readonly logger = new Logger(SeedPokemonTable1684581753124.name, {
    timestamp: true,
  });

  public async up(queryRunner: QueryRunner): Promise<void> {
    // disable logging to console for this migration due to large number of inserts
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    queryRunner.connection.logger.logQuery = () => {};
    const insertIntoPokemon = (pokemons: CreatePokemonDto[]) =>
      queryRunner.manager.insert(Pokemon, pokemons);

    this.logger.log(`Reading seed file...`);
    const workbook = XLSX.readFile(join(__dirname, 'seed.xlsx'));
    const sheetName = workbook.SheetNames[0];
    const sheet = workbook.Sheets[sheetName];
    const data = XLSX.utils.sheet_to_json(sheet);
    this.logger.log(`Read ${data.length} rows from seed file`);

    // chunks of 50 to handle parameter limit of 2100 in MSSQL
    this.logger.log(`Inserting Pokemons...`);
    const CHUNK_SIZE = 50;
    const insertions = [];
    let chunks: CreatePokemonDto[] = [];
    for (const pokemon of data) {
      chunks.push(
        plainToClass(CreatePokemonDto, pokemon, {
          enableImplicitConversion: true,
        }),
      );
      if (chunks.length === CHUNK_SIZE) {
        insertions.push(insertIntoPokemon(chunks));
        chunks = [];
      }
    }
    if (chunks.length > 0) {
      insertions.push(insertIntoPokemon(chunks));
    }
    await Promise.all(insertions);

    this.logger.log(`Pokemons seeded successfully`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    const pokemonRepository = queryRunner.connection.getRepository(Pokemon);

    this.logger.log(`Deleting Pokemons...`);
    await pokemonRepository.delete({});

    this.logger.log(`Pokemons deleted successfully`);
  }
}
