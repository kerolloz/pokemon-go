import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreatePokemonTable1684581388637 implements MigrationInterface {
  name = 'CreatePokemonTable1684581388637';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "pokemon" ("id" int NOT NULL IDENTITY(1,1), "name" nvarchar(255) NOT NULL, "pokedex_number" int NOT NULL, "img_name" nvarchar(255) NOT NULL, "generation" int NOT NULL, "evolution_stage" nvarchar(255), "evolved" int NOT NULL, "family_id" int, "cross_gen" int NOT NULL, "type1" nvarchar(255) NOT NULL, "type2" nvarchar(255), "weather1" nvarchar(255) NOT NULL, "weather2" nvarchar(255), "stat_total" int NOT NULL, "atk" int NOT NULL, "def" int NOT NULL, "sta" int NOT NULL, "legendary" int NOT NULL, "aquireable" int NOT NULL, "spawns" int NOT NULL, "regional" int NOT NULL, "raidable" int NOT NULL, "hatchable" int NOT NULL, "shiny" int NOT NULL, "nest" int NOT NULL, "new" int NOT NULL, "not_gettable" int NOT NULL, "future_evolve" int NOT NULL, "cp40" int NOT NULL, "cp39" int NOT NULL, CONSTRAINT "PK_0b503db1369f46c43f8da0a6a0a" PRIMARY KEY ("id"))`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "pokemon"`);
  }
}
