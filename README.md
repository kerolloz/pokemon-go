# Pokemon Go API 🐹

This is a Node.js API built using NestJS and uses MSSQL for the database. It is a REST API that exposes a few endpoints to manage Pokemon Go data in CRUD fashion.

## Getting Started

### Prerequisites

- Docker 🐋
- Docker Compose 🐳🐳

### Getting Up and Running

1. Clone the repo
2. Copy the `.env.example` file to `.env`. This file contains the environment variables that the app needs to run. You can change the values to your liking.

    ```bash
    cp .env.example .env
    ```

3. Run `docker-compose up` to get everything up and running. This command will start the MSSQL container and the API image will be built from the Dockerfile.
4. Find the API running on <http://localhost:8080>
5. Use Postman or any other REST client to test the endpoints.

The button below will download a Postman collection with the endpoints already configured. Just import the collection into Postman and you're good to go.

[![Run in Postman](https://run.pstmn.io/button.svg)](https://gitlab.com/kerolloz/pokemon-go/-/raw/master/PokemonGo.postman_collection.json?inline=false)

## API Documentation 📖

The API seeds the database from the `seed.xlsx` file. It contains data for 822 Pokemons.

### Entities

#### Pokemon ⚡

A pokemon has the following attributes:

- `id` (Number): The unique identifier of the Pokemon. (Auto-generated)
- `name` (String): The name of the Pokemon.
- `pokedexNumber` (Number): The Pokedex number of the Pokemon.
- `imgName` (String): The image name of the Pokemon.
- `generation` (Number): The generation number in which the Pokemon was introduced.
- `evolutionStage` (String, optional): The evolution stage of the Pokemon.
- `evolved` (Number): Indicates if the Pokemon has evolved or not.
- `familyID` (Number, optional): The ID of the Pokemon's family.
- `crossGen` (Number): Indicates if the Pokemon has cross-generational evolutions.
- `type1` (String): The primary type of the Pokemon.
- `type2` (String, optional): The secondary type of the Pokemon.
- `weather1` (String): The primary weather condition associated with the Pokemon.
- `weather2` (String, optional): The secondary weather condition associated with the Pokemon.
- `statTotal` (Number): The total base stats of the Pokemon.
- `atk` (Number): The base attack stat of the Pokemon.
- `def` (Number): The base defense stat of the Pokemon.
- `sta` (Number): The base stamina/HP stat of the Pokemon.
- `legendary` (Number): Indicates if the Pokemon is legendary.
- `aquireable` (Number): Indicates if the Pokemon is acquirable.
- `spawns` (Number): Indicates if the Pokemon spawns in the wild.
- `regional` (Number): Indicates if the Pokemon is regional.
- `raidable` (Number): Indicates if the Pokemon can appear in raids.
- `hatchable` (Number): Indicates if the Pokemon can hatch from eggs.
- `shiny` (Number): Indicates if the Pokemon has a shiny variant.
- `nest` (Number): Indicates if the Pokemon nests in specific areas.
- `new` (Number): Indicates if the Pokemon is newly introduced.
- `notGettable` (Number): Indicates if the Pokemon is unobtainable.
- `futureEvolve` (Number): Indicates if the Pokemon has future evolutions.
- `cp40` (Number): The maximum combat power at level 40.
- `cp39` (Number): The maximum combat power at level 39.

### Endpoints 🚀

#### `GET /pokemons`

Returns a paginated list of all pokemons.
It supports the following query parameters:

- `page` - The page number to return. Defaults to 1.
- `limit` - The number of items to return per page. Defaults to 10. Max is 50.
- `q[name]` - Filter by pokemon name.
- `q[type1]` - Filter by pokemon type1.
- `q[weather1]` - Filter by weather1.
- `q[pokedexNumber]` - Filter by pokedex number.

<details>
<summary>Response</summary>

```json
{
    "items": [
        {
            "id": 1,
            "name": "Electrode",
            "pokedexNumber": 101,
            "imgName": "101",
            "generation": 1,
            "evolutionStage": "2",
            "evolved": 1,
            "familyID": 45,
            "crossGen": 0,
            "type1": "electric",
            "type2": null,
            "weather1": "Rainy",
            "weather2": null,
            "statTotal": 472,
            "atk": 173,
            "def": 179,
            "sta": 120,
            "legendary": 0,
            "aquireable": 1,
            "spawns": 1,
            "regional": 0,
            "raidable": 0,
            "hatchable": 0,
            "shiny": 0,
            "nest": 0,
            "new": 0,
            "notGettable": 0,
            "futureEvolve": 0,
            "cp40": 1900,
            "cp39": 1873
        }
    ],
    "meta": {
        "totalItems": 823,
        "itemCount": 1,
        "itemsPerPage": 1,
        "totalPages": 823,
        "currentPage": 1
    }
}
```

</details>

---

#### `GET /pokemons/:id`

Returns a single pokemon by id.

<details>
<summary>Response</summary>

```json
{
    "id": 81,
    "name": "Nidoqueen",
    "pokedexNumber": 31,
    "imgName": "31",
    "generation": 1,
    "evolutionStage": "3",
    "evolved": 1,
    "familyID": 12,
    "crossGen": 0,
    "type1": "poison",
    "type2": "ground",
    "weather1": "Cloudy",
    "weather2": "Sunny/clear",
    "statTotal": 534,
    "atk": 180,
    "def": 174,
    "sta": 180,
    "legendary": 0,
    "aquireable": 1,
    "spawns": 1,
    "regional": 0,
    "raidable": 0,
    "hatchable": 0,
    "shiny": 0,
    "nest": 0,
    "new": 0,
    "notGettable": 0,
    "futureEvolve": 0,
    "cp40": 2338,
    "cp39": 2304
}
```

</details>

---

#### `POST /pokemons`

Creates a new pokemon.

<details>
<summary>Request Body</summary>

```js
{
  "name": "Zeeko",
  "pokedexNumber": 93,
  "imgName": "93",
  "generation": 9,
  "evolutionStage": "1",
  "evolved": 0,
  "familyID": 41,
  "crossGen": 0,
  "type1": "ghost",
  "type2": "poison",
  "weather1": "Fog",
  "weather2": "Cloudy",
  "statTotal": 425,
  "atk": 223,
  "def": 112,
  "sta": 90,
  "legendary": 0,
  "aquireable": 1,
  "spawns": 1,
  "regional": 0,
  "raidable": 0,
  "hatchable": 0,
  "shiny": 0,
  "nest": 0,
  "new": 0,
  "notGettable": 0,
  "futureEvolve": 0,
  "cp40": 1716,
  "cp39": 1692
}
```

</details>

Response: Same as Request Body, but returns the newly created pokemon with an auto-generated id.

---

#### `PATCH /pokemons/:id`

Updates a pokemon by id.

Request Body: Same as POST /pokemons endpoint. (No need to specify all fields, only the ones you want to update).

Response: returns the updated pokemon.

---

#### `DELETE /pokemons/:id`

Deletes a pokemon by id.

Response: No response body. Returns 204 status code if successful.
